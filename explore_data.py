import numpy as np
import pandas as pd
#import seaborn as sns
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from utils.torch_train_utils import *
X=pd.read_csv('data/blood_tests_filled.csv')
X.set_index('ids',inplace=True)
num_feats=X.shape[1]-1
num_samples=X.shape[0]
num_positives=X['target'].sum()/num_samples
#X.describe()
col_means=X.mean()
col_vars=X.var()
print("some statistics:")
print("only first 5 columns:")
print('col_means')
print(X.mean()[:5])
print('col_vars')
print(X.var()[:5])
X.plot.box()
plt.show()

scaler = StandardScaler()
X_np = scaler.fit_transform(X)
X=pd.DataFrame(X_np,columns=X.columns)
print("after normalization:")
print("some statistics:")
print("only first 5 columns:")
print('col_means')
print(X.mean()[:5])
print('col_vars')
print(X.var()[:5])
a=7
