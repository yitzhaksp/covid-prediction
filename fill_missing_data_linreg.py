import pandas as pd
from sklearn.linear_model import LinearRegression
import numpy as np
df=pd.read_csv('data/blood_tests_forplay_fillmissfeat.csv')
df.drop("ids",axis=1,inplace=True)
print(df.head())
alp=df.ALP
alp_nan=alp[alp.isnull()]
df_alp_nan=df[df.ALP.isnull()]
df_alp_notnan=df.dropna()
xtrain=df_alp_notnan.drop("ALP",axis=1)
ytrain=df_alp_notnan.ALP
xtest=df_alp_nan.drop("ALP",axis=1)
model=LinearRegression()
model.fit(xtrain,ytrain)
y_pred = model.predict(xtest)
alp_filled=df.ALP.copy()
i=0
for ind, x in alp.items():
    if np.isnan(x):
        alp_filled.loc[ind]=y_pred[i]
        i+=1
a=7