import pandas as pd
from sklearn.linear_model import LinearRegression
import numpy as np
df=pd.read_csv('data/blood_tests_small.csv')
print(df.head())
for col_name in df:
    if col_name!='ids':
        this_col=df[col_name]
        fill_value=this_col.median()
        print('median of col: '+str(fill_value))
        this_col[this_col.isnull()]=fill_value
df.to_csv('data/blood_tests_small_filled.csv')

a=7