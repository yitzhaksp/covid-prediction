#from time import time
#import matplotlib as mpl
#import matplotlib.cm as cm
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
import sklearn
from sklearn.model_selection import train_test_split
import pandas as pd
import sklearn.metrics as metr
classifier_type=2
data=pd.read_csv('data/blood_tests_filled.csv')
data.set_index('ids',inplace=True)
y=data['target']
X=data.drop('target',axis=1)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
if classifier_type==1:
    nb = MultinomialNB() # instantiate a Multinomial Naive Bayes model
    nb.fit(X_train, y_train) # train the model(timing it with an IPython "magic command")
    y_pred = nb.predict(X_test) # make class predictions for X_test_dtm
elif classifier_type==2:
    logreg = LogisticRegression(class_weight="balanced")  # instantiate a logistic regression model
    logreg.fit(X_train, y_train)  # fit the model with training data
    y_pred = logreg.predict(X_test)
elif classifier_type == 3:
    classifier = LinearSVC(class_weight='balanced')  # instantiate a logistic regression model
    classifier.fit(X_train, y_train)  # fit the model with training data
    y_pred = classifier.predict(X_test)
print("precision: {:.3} ".format( metr.precision_score(y_test, y_pred)))
print("recall: {:.3}".format( metr.recall_score(y_test, y_pred)))
print("F1: {:.3}".format( metr.f1_score(y_test, y_pred)))

cnf_matrix = metr.confusion_matrix(y_test, y_pred)
from utils.analysis_utils import *
plt.figure(figsize=(8,6))
plot_confusion_matrix(cnf_matrix, classes=['Not Relevant','Relevant'],normalize=True,
                      title='Confusion matrix with normalization')

a=7