# https://github.com/tensorflow/text/blob/master/docs/tutorials/classify_text_with_bert.ipynb

import os
import shutil

import tensorflow as tf
import tensorflow_hub as hub
import tensorflow_text as text
#from official.nlp import optimization  # to create AdamW optimizer
import matplotlib.pyplot as plt
import transformers


tf.get_logger().setLevel('ERROR')
from bert_models import *
from example_1_utils import *
################################################
# split into test and val
################################################

AUTOTUNE = tf.data.AUTOTUNE
batch_size = 32
seed = 42
train_dir='/home/yitzhak/PycharmProjects/lung_project/examples/example_1/aclImdb/train'
test_dir='/home/yitzhak/PycharmProjects/lung_project/examples/example_1/aclImdb/test'

raw_train_ds = tf.keras.utils.text_dataset_from_directory(
    train_dir,
    batch_size=batch_size,
    validation_split=0.2,
    subset='training',
    seed=seed)

class_names = raw_train_ds.class_names
train_ds = raw_train_ds.cache().prefetch(buffer_size=AUTOTUNE)

val_ds = tf.keras.utils.text_dataset_from_directory(
    train_dir,
    batch_size=batch_size,
    validation_split=0.2,
    subset='validation',
    seed=seed)

val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

test_ds = tf.keras.utils.text_dataset_from_directory(
    test_dir,
    batch_size=batch_size)

test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

#check:
for text_batch, label_batch in train_ds.take(1):
  for i in range(3):
    print(f'Review: {text_batch.numpy()[i]}')
    label = label_batch.numpy()[i]
    print(f'Label : {label} ({class_names[label]})')

################################################
# create bert model
################################################

bert_model_name = 'small_bert/bert_en_uncased_L-4_H-512_A-8'
tfhub_handle_encoder = map_name_to_encoder[bert_model_name]
tfhub_handle_preprocess = map_name_to_preprocess[bert_model_name]
print(f'BERT model selected           : {tfhub_handle_encoder}')
print(f'Preprocess model auto-selected: {tfhub_handle_preprocess}')

################################################
# preprocess text for bert model
################################################
text_test = ['זה סרט כל כך מדהים!']
if True:
    print('building preprocess model ...')
    bert_preprocess_model = hub.KerasLayer(tfhub_handle_preprocess)
    #bert_preprocess_model_2 = transformers.BertTokenizerFast.from_pretrained("avichr/heBERT_sentiment_analysis")
    #bert_preprocess_model_3 = hub.KerasLayer("avichr/heBERT_sentiment_analysis")
    #check:
    text_preprocessed = bert_preprocess_model(text_test)
    #text_preprocessed_2 = bert_preprocess_model_2(text_test)
    #text_preprocessed_3 = bert_preprocess_model_3(text_test)

    print(f'Keys       : {list(text_preprocessed.keys())}')
    print(f'Shape      : {text_preprocessed["input_word_ids"].shape}')
    print(f'Word Ids   : {text_preprocessed["input_word_ids"][0, :12]}')
    print(f'Input Mask : {text_preprocessed["input_mask"][0, :12]}')
    print(f'Type Ids   : {text_preprocessed["input_type_ids"][0, :12]}')
################################################
# obtain outputs of bert model
################################################
#check:
if False:
    print('building bert model ...')
    bert_model = hub.KerasLayer(tfhub_handle_encoder)
    print("processing text with BERT ...")
    bert_results = bert_model(text_preprocessed)
    print(f'Loaded BERT: {tfhub_handle_encoder}')
    print(f'Pooled Outputs Shape:{bert_results["pooled_output"].shape}')
    print(f'Pooled Outputs Values:{bert_results["pooled_output"][0, :12]}')
    print(f'Sequence Outputs Shape:{bert_results["sequence_output"].shape}')
    print(f'Sequence Outputs Values:{bert_results["sequence_output"][0, :12]}')
###############################################
# define my model with Bert in its core
##############################################
print('building classifier model ...')
classifier_model = build_classifier_model(prepr_handle=tfhub_handle_preprocess,
                                          enc_handle=tfhub_handle_encoder)
print("processing text with classifier ...")
bert_raw_result = classifier_model(tf.constant(text_test))
#p=tf.sigmoid(bert_raw_result)
print('classification result:')
print(bert_raw_result)
