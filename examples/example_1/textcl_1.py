# https://github.com/tensorflow/text/blob/master/docs/tutorials/classify_text_with_bert.ipynb

import os
import shutil

import tensorflow as tf
import tensorflow_hub as hub
import tensorflow_text as text
#from official.nlp import optimization  # to create AdamW optimizer
import matplotlib.pyplot as plt

tf.get_logger().setLevel('ERROR')
from bert_models import *
from example_1_utils import *
################################################
# split into test and val
################################################

AUTOTUNE = tf.data.AUTOTUNE
batch_size = 32
seed = 42
train_dir='/home/yitzhak/PycharmProjects/lung_project/examples/example_1/aclImdb/train'
test_dir='/home/yitzhak/PycharmProjects/lung_project/examples/example_1/aclImdb/test'

raw_train_ds = tf.keras.utils.text_dataset_from_directory(
    train_dir,
    batch_size=batch_size,
    validation_split=0.2,
    subset='training',
    seed=seed)

class_names = raw_train_ds.class_names
train_ds = raw_train_ds.cache().prefetch(buffer_size=AUTOTUNE)

val_ds = tf.keras.utils.text_dataset_from_directory(
    train_dir,
    batch_size=batch_size,
    validation_split=0.2,
    subset='validation',
    seed=seed)

val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

test_ds = tf.keras.utils.text_dataset_from_directory(
    test_dir,
    batch_size=batch_size)

test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

#check:
for text_batch, label_batch in train_ds.take(1):
  for i in range(3):
    print(f'Review: {text_batch.numpy()[i]}')
    label = label_batch.numpy()[i]
    print(f'Label : {label} ({class_names[label]})')

################################################
# create bert model
################################################

bert_model_name = 'small_bert/bert_en_uncased_L-4_H-512_A-8'
tfhub_handle_encoder = map_name_to_encoder[bert_model_name]
tfhub_handle_preprocess = map_name_to_preprocess[bert_model_name]
print(f'BERT model selected           : {tfhub_handle_encoder}')
print(f'Preprocess model auto-selected: {tfhub_handle_preprocess}')

################################################
# preprocess text for bert model
################################################
text_test = ['this is such an amazing movie!']
if False:
    print('building preprocess model ...')
    bert_preprocess_model = hub.KerasLayer(tfhub_handle_preprocess)
#check:
    text_preprocessed = bert_preprocess_model(text_test)
    print(f'Keys       : {list(text_preprocessed.keys())}')
    print(f'Shape      : {text_preprocessed["input_word_ids"].shape}')
    print(f'Word Ids   : {text_preprocessed["input_word_ids"][0, :12]}')
    print(f'Input Mask : {text_preprocessed["input_mask"][0, :12]}')
    print(f'Type Ids   : {text_preprocessed["input_type_ids"][0, :12]}')
################################################
# obtain outputs of bert model
################################################
#check:
if False:
    print('building bert model ...')
    bert_model = hub.KerasLayer(tfhub_handle_encoder)
    print("processing text with BERT ...")
    bert_results = bert_model(text_preprocessed)
    print(f'Loaded BERT: {tfhub_handle_encoder}')
    print(f'Pooled Outputs Shape:{bert_results["pooled_output"].shape}')
    print(f'Pooled Outputs Values:{bert_results["pooled_output"][0, :12]}')
    print(f'Sequence Outputs Shape:{bert_results["sequence_output"].shape}')
    print(f'Sequence Outputs Values:{bert_results["sequence_output"][0, :12]}')
###############################################
# define my model with Bert in its core
##############################################
print('building classifier model ...')
classifier_model = build_classifier_model(prepr_handle=tfhub_handle_preprocess,
                                          enc_handle=tfhub_handle_encoder)
print("processing text with classifier ...")
bert_raw_result = classifier_model(tf.constant(text_test))
#p=tf.sigmoid(bert_raw_result)
print('classification result:')
print(bert_raw_result)
#tf.keras.utils.plot_model(classifier_model)
###############################################
# define loss function
##############################################
loss = tf.keras.losses.BinaryCrossentropy(from_logits=False)
metrics = tf.metrics.BinaryAccuracy()
###############################################
# define optimizer
##############################################

epochs = 5
'''
steps_per_epoch = tf.data.experimental.cardinality(train_ds).numpy()
num_train_steps = steps_per_epoch * epochs
num_warmup_steps = int(0.1*num_train_steps)
init_lr = 3e-5
optimizer = optimization.create_optimizer(init_lr=init_lr,
                                          num_train_steps=num_train_steps,
                                          num_warmup_steps=num_warmup_steps,
                                          optimizer_type='adamw')
'''
optimizer=tf.keras.optimizers.Adam()
classifier_model.compile(optimizer=optimizer,
                         loss=loss,
                         metrics=metrics)
###############################################
# train model
##############################################
print(f'Training model with {tfhub_handle_encoder}')
history = classifier_model.fit(x=train_ds,
                               validation_data=val_ds,
                               epochs=epochs)
###############################################
# evaluate the model
##############################################
loss, accuracy = classifier_model.evaluate(test_ds)
print(f'Loss: {loss}')
print(f'Accuracy: {accuracy}')
history_dict = history.history
print(history_dict.keys())

acc = history_dict['binary_accuracy']
val_acc = history_dict['val_binary_accuracy']
loss = history_dict['loss']
val_loss = history_dict['val_loss']

epochs = range(1, len(acc) + 1)
fig = plt.figure(figsize=(10, 6))
fig.tight_layout()

plt.subplot(2, 1, 1)
# r is for "solid red line"
plt.plot(epochs, loss, 'r', label='Training loss')
# b is for "solid blue line"
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
# plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.subplot(2, 1, 2)
plt.plot(epochs, acc, 'r', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend(loc='lower right')