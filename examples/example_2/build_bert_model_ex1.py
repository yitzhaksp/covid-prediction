import shap
import transformers
import torch
import numpy as np
import scipy as sp
import pandas as pd
#########################################
# load data
########################################
from datasets import load_dataset
text  = 'אני ממש אוהב את העולם.'
from transformers import AutoTokenizer, AutoModel, pipeline


#################################
# build bert model (pytorch)
################################
# load a BERT sentiment analysis model
print('text= '+text)
print('building tokenizer for BERT ...')
bert_tokenizer = transformers.BertTokenizerFast.from_pretrained("avichr/heBERT_sentiment_analysis")
print('building BERT model ...')
bert_model1 = transformers.BertForSequenceClassification.from_pretrained("avichr/heBERT_sentiment_analysis")
bert_model2 = transformers.BertModel.from_pretrained("avichr/heBERT_sentiment_analysis")
bert_input = bert_tokenizer(text,return_tensors="pt")
print('bert input : ')
print(bert_input)
with torch.no_grad():
    bert_outp1 = bert_model1(**bert_input)
    bert_outp2 = bert_model2(**bert_input)

print('bert output: ')
print(bert_outp1)
logits=bert_outp2.logits
a=7