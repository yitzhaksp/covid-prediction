# https://huggingface.co/docs/transformers/model_doc/bert#transformers.BertForSequenceClassification

import torch
from transformers import BertTokenizer, BertForSequenceClassification
print('building tokenizer ...')
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
print('building BERT model ...')
model = BertForSequenceClassification.from_pretrained("bert-base-uncased")

inputs = tokenizer("Hello, my dog is cute", return_tensors="pt")
print('bert_input:')
print(inputs)
with torch.no_grad():
    outp=model(**inputs)

logits=outp.logits
predicted_class_id = logits.argmax().item()
model.config.id2label[predicted_class_id]
