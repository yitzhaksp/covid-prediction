import pandas as pd
import torch
import numpy as np
from torch import nn
from utils_part1 import *
from utils_part2 import *

#######################################
# load data
#######################################
prune_data=True
small_size=500
datapath = 'data/bbc-text.csv'
df = pd.read_csv(datapath)
print("head of data: ")
print(df.head())
if prune_data:
    df=df[:small_size]
np.random.seed(112)
df_train, df_val, df_test = np.split(df.sample(frac=1, random_state=42),
                                     [int(.8*len(df)), int(.9*len(df))])
print('sizes of train/val/test sets: ')
print(len(df_train),len(df_val), len(df_test))
#df.groupby(['category']).size().plot.bar()
####################################
# define tokenizer (to preprocess data for BERT)
# and labels
######################################
print('building tokenizer ...')
tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
labels = {'business':0,
          'entertainment':1,
          'sport':2,
          'tech':3,
          'politics':4
          }
EPOCHS = 5
model = BertClassifier()
LR = 1e-6
print('training ...')
train(model, df_train, df_val, LR, EPOCHS,labels,tokenizer)
