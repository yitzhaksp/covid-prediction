import numpy as np
import pandas as pd
#import seaborn as sns
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
import sklearn.metrics as metr
from utils.torch_train_utils import *
import random

results_dir='results/'
dbg_flg=False
EPOCHS =20
BATCH_SIZE = 10
LEARNING_RATE = 0.001
patience=10
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)
if dbg_flg:
    EPOCHS=1

# fix random seed for replication of training results
seed = 1
torch.manual_seed(seed)
torch.cuda.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
np.random.seed(seed)
random.seed(seed)
torch.backends.cudnn.benchmark = False
torch.backends.cudnn.deterministic = True


data=pd.read_csv('data/blood_tests_filled.csv')
data.set_index('ids',inplace=True)
y=data['target']
X=data.drop('target',axis=1)
num_feats=X.shape[1]
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, random_state=1)
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_val = scaler.transform(X_val)
X_test = scaler.transform(X_test)
X_val, y_val= torch.FloatTensor(X_val),torch.FloatTensor(y_val)
train_data = TrainData(torch.FloatTensor(X_train),
                       torch.FloatTensor(y_train))
test_data = TestData(torch.FloatTensor(X_test))
train_loader = DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(dataset=test_data, batch_size=1)
model = BinaryClassification(num_feats=num_feats)
model.to(device)
print(model)
criterion = nn.BCEWithLogitsLoss()
optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE)
model.train()
counter=-1
loss_val_prev = 0
for e in range(1, EPOCHS + 1):
    epoch_loss = 0
    epoch_acc = 0
    for X_batch, y_batch in train_loader:
        X_batch, y_batch = X_batch.to(device), y_batch.to(device)
        optimizer.zero_grad()
        y_pred = model(X_batch)
        loss = criterion(y_pred, y_batch.unsqueeze(1))
        acc = binary_acc(y_pred, y_batch.unsqueeze(1))
        loss.backward() # compute the gradient. required for the optimization step.
        optimizer.step() #optimization step
        epoch_loss += loss.item()
        epoch_acc += acc.item()
    y_val_pred = model(X_val)
    loss_val = criterion(y_val_pred, y_val.unsqueeze(1))
    print(f'Epoch {e + 0:03}: | Loss: {epoch_loss / len(train_loader):.5f} |Val_loss: {loss_val:.5f}:| '
          f'Acc: {epoch_acc / len(train_loader):.3f}'  )
    if loss_val >= loss_val_prev:
        counter+=1
    if counter==patience:
        print('early stopping ...')
        break
    loss_val_prev=loss_val

model.eval() #set model to evaluation mode
y_val_pred = model(X_val)
y_val_pred=torch.sigmoid(y_val_pred)
with torch.no_grad():
    y_val_pred=y_val_pred.cpu().numpy().squeeze()
    y_val=y_val.cpu().numpy().squeeze()

np.save(results_dir+'y_val.npy',y_val)
np.save(results_dir+'y_val_pred.npy',y_val_pred)
y_val_pred=np.round(y_val_pred)
print('00000000000000000000000000000000000000000000000000000')
print('on val set:')
print("precision: {:.3} ".format( metr.precision_score(y_val, y_val_pred)))
print("recall: {:.3}".format( metr.recall_score(y_val, y_val_pred)))
print("F1: {:.3}".format( metr.f1_score(y_val, y_val_pred)))
y_pred_list = []
with torch.no_grad():
    for X_batch in test_loader:
        X_batch = X_batch.to(device)
        y_test_pred_batch = model(X_batch)
        y_test_pred_batch = torch.sigmoid(y_test_pred_batch)
        #y_pred_tag = torch.round(y_test_pred)
        y_pred_list.append(y_test_pred_batch.cpu().numpy())
y_test_pred = np.array([a.squeeze().tolist() for a in y_pred_list]).squeeze()
np.save(results_dir+'y_test_pred.npy',y_test_pred)
y_test_pred=np.round(y_test_pred)
y_test=y_test.to_numpy()
np.save(results_dir+'y_test.npy',y_test)
print('00000000000000000000000000000000000000000000000000000')
print('on test set:')
print("precision: {:.3} ".format( metr.precision_score(y_test, y_test_pred)))
print("recall: {:.3}".format( metr.recall_score(y_test, y_test_pred)))
print("F1: {:.3}".format( metr.f1_score(y_test, y_test_pred)))
conf_matr=confusion_matrix(y_test, y_test_pred)
#print(classification_report(y_test, y_pred_list))

a=7