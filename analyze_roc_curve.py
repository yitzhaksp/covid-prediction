import numpy as np
import sklearn.metrics as metr
from utils.analysis_utils import *


results_dir='results/'
y_val=np.load(results_dir+'y_val.npy')
y_val_pred=np.load(results_dir+'y_val_pred.npy')
y_test=np.load(results_dir+'y_test.npy')
y_test_pred=np.load(results_dir+'y_test_pred.npy')

fpr,tpr,tresholds=metr.roc_curve(y_test,y_test_pred)
f,ax=plot_roc_cur(fpr,tpr)
plt.show()
f.savefig('plots/roc_curve.png')

tresholds=0.1*np.arange(10)
for trh in tresholds:
    y_val_pred=y_val_pred>trh
    y_test_pred_bin=y_test_pred>trh
    print('00000000000000000000000000000000000000000000000000000')
    print('00000000000000000000000000000000000000000000000000000')
    print('00000000000000000000000000000000000000000000000000000')
    print('treshold: {:.2}'.format(trh))
    print('on test set:')
    print("precision: {:.3} ".format( metr.precision_score(y_test, y_test_pred_bin)))
    print("recall: {:.3}".format( metr.recall_score(y_test, y_test_pred_bin)))
    print("F1: {:.3}".format( metr.f1_score(y_test, y_test_pred_bin)))
# debug purposes (end)

a=7